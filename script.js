let currentPlayer = 'X';
let nextPlayer = 'O';

let playerXSelections = [];
let playerOSelections = [];

const winingCombinations = [
  [1,2,3],
  [4,5,6],
  [7,8,9],
  [1,4,7],
  [2,5,8],
  [3,6,9],
  [1,5,9],
  [3,5,7]
]

const handleClick = function(event) {
  const cell = event.target;
  
  cell.innerHTML = currentPlayer;

  if (currentPlayer === 'X') {
    playerSelections = playerXSelections;
    nextPlayer = '0'
  } else {
    playerSelections = playerOSelections;
    nextPlayer = 'X';
  }
  
  playerSelections.push(Number(cell.id));

  if (checkWinner(playerSelections)) {
    alert('Player ' + currentPlayer + ' wins!');
    resetGame();
  }

  if (checkDraw()) {
    alert('Draw!');
    resetGame();
  }


  currentPlayer = nextPlayer;

  
}

const cells = document.querySelectorAll('td');

for (let i = 0; i < cells.length; i++) {
  cells[i].addEventListener('click', handleClick);
}

function checkWinner () {
   

  for (let i = 0; i < winingCombinations.length; i++) {
   let matches = 0;
    for (let j = 0; j < winingCombinations[i].length; j++) {
      if (playerSelections.includes(winingCombinations[i][j])) {
        matches++
      }
    }
  
    if (matches === 3){
      return true;
    }
    
  }
  return false
}

function checkDraw() {
  return (playerOSelections.length + playerXSelections.length) >= cells.length;
}

function resetGame() {
  playerXSelections = new Array();
  playerOSelections = new Array();
  for (let i = 0; i < cells.length; i++) {
      cells[i].innerHTML = '';
  }
}